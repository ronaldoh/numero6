/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.Inscricao;
import if6ae.entity.Inscricao_;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author ronaldo
 */
@Named(value = "inscricaominicursojpa")
@SessionScoped
public class InscricaoMinicursoJpa implements Serializable {

    @PersistenceContext
    EntityManager em;

    public InscricaoMinicursoJpa() {
    }

    public List<Inscricao> findInscricaoMinicursoByNumero(int numeroInscr) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Inscricao> cq = cb.createQuery(Inscricao.class);
        Root<Inscricao> rt = cq.from(Inscricao.class);
        cq.where(cb.equal(rt.get(Inscricao_.numero), numeroInscr));
        TypedQuery<Inscricao> q = em.createQuery(cq);
        return q.getResultList();
    }

}
